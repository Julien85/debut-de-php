<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>
<body>

<h1>Mon premier mélange entre HTML et PHP</h1>

<form method="GET">
    <!-- écrire le code du formulaire ici qui sera un champ de texte et un bouton pour envoyer -->
  <label for="GET-name">Saisir la valeur :</label>
  <input type="text" name="random" placeholder="Ecrire le texte ici">
  <input type="submit" value="Envoyer">

</form>

<!-- écrire le code après ce commentaire --> 

<?php

    $get = $_GET['random'];
    
    // Récuperer la valeur saisie dans le formulaire html et compter et afficher le nombre de caractères 
    // Avec une boucle, afficher une liste déroulante de la taille du nombre de caractères 
    // ( Si mot de 10 caractères, faire une liste déroulante de 1 à 10)
    
    // Garder la chaine de caractère en stock, supprimer lui toutes les voyelles et afficher la
    // Si vous avez des apostrophes (') dans votre chaine, affichez la chaine avec les caractères échappés (\')
    
    // Compter le nombre de caractères restant après la suppression des voyelles et faites la différence entre
    // le nombre de caractères à l'entrée et à la sortie.
    
    // Faites une condition en fonction du nombre de caractères supprimés: de 0 à 5 affiche : Pas beaucoup de voyelles. sinon : Belle suppression
    
    // Bien sûr, afin de facilité la lecture de votre page, vous mettrez en forme avec de beaux blocs qui détermineront
    // qui fait quoi ( exemple : <h2>Ma liste déroulante</h2> et la liste en dessous... )
    
    // Afin de facilité la lecture des fichiers, utilisez la function include
    $count = strlen($get);
    $voyelle = array("a", "e", "i", "o", "u", "y");

    echo "La valeur saisie est de : $count caractères dans $get <br><br>"; 
?>
<h2>Mon menu déroulant : 
    <select> 
        <?php
            $x = 0;
            while($x < $count){$x++;
            echo "<option>choix $x</option><br>";
            }
        ?>        
    </select>
</h2>

<?php
// facon de retirer les voyelles dans la chaine de caractere $get AVEC un tableau
    $voyelle2 = str_replace($voyelle, '', $get);
    echo "$voyelle2 <br><br>";
/* facon de retirer les voyelles dans la chaine de caractere $get SANS tableau
    $voyelle-a = str_replace("a", "", $get);
    $voyelle-e = str_replace("e", "", $voyelle-a);
    $voyelle-i = str_replace("i", "", $voyelle-e);
    $voyelle-o = str_replace("o", "", $voyelle-i);
    $voyelle-u = str_replace("u", "", $voyelle-o);
    $voyelle-y = str_replace("y", "", $voyelle-u);
*/
    $voyelle3 = strlen($voyelle2);
    $caractere = $count - $voyelle3;


    echo 'Nb de lettres restantes sans voyelles :' . $voyelle3 . '<br>' .
     'Nb de voyelles supprimées : ' . $caractere . '<br>';

    if ($voyelle >=6){
        echo 'pas bcp de voyelles';
    }else {
        echo 'belle suppression';
    }
?>

<!-- écrire le code avant ce commentaire -->

</body>
</html>
