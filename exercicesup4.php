<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>
<body>

<?php

// Ecrire le code permettant d'afficher 5 messages différents avec la fonction Switch, 
// suivant les goûts musicaux de chacun : Rap, Jazz, Techno, Classique et autre
// Vous changerez manuellement le contenu de la variable $a pour afficher vos différents messages

    
$a = "Rap";
    
?>
    
<!-- écrire le code avant ce commentaire -->
<?php    
    switch ($a) // on indique sur quelle variable on travaille
    { 
        case "Techno": // dans le cas où $note vaut 0
            echo "Tu es vraiment un gros nul !!!";
        break;
        
        case "Rap": // dans le cas où $note vaut 5
            echo "Tu es très mauvais";
        break;
        
        case "Jazz": // dans le cas où $note vaut 7
            echo "C'est naz";
        break;
        
        case "Classique": // etc. etc.
            echo "Pour s'endormir";
        break;

        default:
            echo "Désolé, je n'ai pas de message à afficher pour cette musique";
    }

?>


<!-- écrire le code avant ce commentaire -->

</body>
</html>
